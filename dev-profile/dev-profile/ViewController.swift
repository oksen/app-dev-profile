//
//  ViewController.swift
//  dev-profile
//
//  Created by Omar PC on 9/3/18.
//  Copyright © 2018 Omar PC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var profileImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImg.layer.cornerRadius = 3
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

